let Token = artifacts.require("./StandardToken.sol");
let TestContract = artifacts.require("./TestContract.sol");

contract("TestContract", (accounts) => {
    const totalSupply = 1000;
    const payees = accounts.slice(0, accounts.length - 2);
    const fundAmount = 100;

    let token;
    let testContract;

    beforeEach(async () => {
        // Expected to have `totalSupply` on `accounts[0]`
        token = await Token.new(totalSupply, { from: accounts[0] });
        testContract = await TestContract.new(payees, token.address, { from: accounts[0] });
    });

    it("try to call fund with empty balance", async () => {
        let result = await testContract.fund.call(fundAmount, { from: accounts[0] });
        await testContract.fund(fundAmount, { from: accounts[0] })
        assert.isNotOk(result.valueOf());
    });

    it("fund the contract with the amount of token", async () => {
        await token.transfer(testContract.address, fundAmount, { from: accounts[0] });
        let result = await testContract.fund.call(fundAmount, { from: accounts[0] });
        assert.isOk(result.valueOf());
        await testContract.fund(fundAmount, { from: accounts[0] });
        let balance = await token.balanceOf.call(testContract.address, { from: accounts[0] });
        assert.equal(balance.valueOf(), fundAmount);
    });

    it("withdraw from the contract", async () => {
        await token.transfer(testContract.address, fundAmount, { from: accounts[0] });
        await testContract.fund(fundAmount, { from: accounts[0] });
        let oldBalances = [];
        for (let i = 0; i < payees.length; i++) {
            let balance = await token.balanceOf.call(payees[i], { from: payees[i] });
            oldBalances.push(balance);
        }
        for (let i = 0; i < payees.length; i++) {
            let result = await testContract.withdraw.call({ from: payees[i]});
            assert.isOk(result.valueOf());
            await testContract.withdraw({ from: payees[i]});
        }
        for (let i = 0; i < payees.length; i++) {
            let balance = await token.balanceOf.call(payees[i], { from: payees[i] });
            assert.equal(balance.valueOf(), parseInt(oldBalances[i]) + Math.floor(fundAmount / payees.length));
        }
        let balance = await token.balanceOf.call(testContract.address, { from: accounts[0] });
        assert.equal(balance.valueOf(), fundAmount - Math.floor(fundAmount / payees.length) * payees.length);
    });

    it("withdraw to a malicious address", async () => {
        let malicious = accounts[accounts.length - 1];
        await token.transfer(testContract.address, fundAmount, { from: accounts[0] });
        await testContract.fund(fundAmount, { from: accounts[0] });
        for (let i = 0; i < payees.length; i++) {
            assert.notEqual(malicious, payees[i]);
        }
        let result = await testContract.withdraw.call({ from: malicious });
        assert.isNotOk(result.valueOf());
        await testContract.withdraw({ from: malicious });
        let maliciousBalance = await token.balanceOf.call(malicious, { from: malicious });
        assert.equal(maliciousBalance.valueOf(), 0);
        let contractBalance = await token.balanceOf.call(testContract.address, { from: accounts[0] });
        assert.equal(contractBalance.valueOf(), fundAmount);
    });
});
