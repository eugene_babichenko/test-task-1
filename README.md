## Task

Write a contract, with a constructor that takes 2 arguments: an address of ERC20
token and an array of address (wallets of users).

Whenever function fund is called on the contract, the amount of ether send to
the contract is split equally to all addresses given in the constructor.

Ether can be withdrawn by user using appropriate wallet with withdraw function.

Please include tests in solutions and make sure funds are secure in the contract
(e.g. can't be withdrawn by undesirable entities).

## Git tags

* `v1` - naive implementation of the needed behavior
* `v2` - reduces gas usage on `fund` calls with a large amount of addresses
passed
* `v3` - cover possible `out of gas` error in the constructor

## Running tests

* Install the dependencies
* `$(npm bin)/testrpc` to run the test server
* Open a new terminal and run `npm test` 

`$(npm bin)/truffle migrate` to run migrations.
