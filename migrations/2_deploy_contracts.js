let StandardToken = artifacts.require("./StandardToken.sol");
let TestContract = artifacts.require("./TestContract.sol");

module.exports = function(deployer) {
    deployer.deploy(StandardToken, 1000).then(() => {
        deployer.deploy(TestContract, [], StandardToken.address);
    });
};
