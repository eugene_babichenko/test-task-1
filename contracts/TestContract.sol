pragma solidity ^0.4.8;

// ERC20 compilant token interface
import "./Token.sol";

// The contract which implements the test task
contract TestContract {
    struct Payee {
        bool allowed;
        uint256 withdrawn;
        bool resetPerformed;
    }

    mapping (address => Payee) payees;

    Token token;
    uint256 payeesN;
    uint256 totalFund;
    
    uint256 resetThreshold;
    uint256 nUponThreshold;
    bool performReset;
    uint256 nResetsPerformed;

    address[] payeesList;
    uint256 payeesReady;
    uint256 initGasLimit;
    bool initialized;

    event Fund(address indexed _caller, uint256 _amount, uint256 _supply);

    function initialize() returns (bool) {
        while (payeesReady < payeesN && msg.gas > initGasLimit) {
            payees[payeesList[payeesReady]].allowed = true;
            payeesReady += 1;
        }
        if (payeesN == payeesReady) initialized = true;
        return initialized;
    }

    function TestContract(address[] _payees, Token _token) {
        initGasLimit = 200000;
        token = _token;
        payeesN = _payees.length;
        payeesList = _payees;
        initialize();
    }

    modifier isInitialized() {
        if (!initialized) throw;
        _;
    }

    // fund this contract with `_amount` of `token`
    function fund(uint256 _amount) isInitialized returns (bool) {
        uint256 _supply = token.balanceOf(this);
        if (_amount <= _supply) {
            totalFund += _amount / payeesN;
            Fund(msg.sender, _amount / payeesN, totalFund);
            return true;
        }
        else {
            return false;
        }
    }

    function withdraw() isInitialized returns (bool) {
        if (performReset && !payees[msg.sender].resetPerformed) {
            payees[msg.sender].withdrawn -= resetThreshold;
            nResetsPerformed += 1;
            payees[msg.sender].resetPerformed = true;
            if (nResetsPerformed == payeesN) {
                resetThreshold = 0;
                nUponThreshold = 0;
                performReset = false;
                nResetsPerformed = 0;
            }
        }

        if (payees[msg.sender].allowed) {
            if (!token.transfer(msg.sender, totalFund - payees[msg.sender].withdrawn)) return false;
            payees[msg.sender].withdrawn = totalFund;
            
            // totalFund related logic is designed to avoid for loop in every fund call (see v1)
            // logic to prevent totalFund overflow
            if (nUponThreshold == 0) {
                resetThreshold = totalFund;
                nUponThreshold = 1;
            }
            else {
                if (!performReset) {
                    nUponThreshold += 1;
                    if (nUponThreshold == payeesN) {
                        performReset = true;
                        totalFund -= resetThreshold;
                    }
                    payees[msg.sender].resetPerformed = false;
                }
            }

            return true;
        }
        else {
            return false;
        }
    }
}
